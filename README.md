<h2>Chat Application</h2>
A web service where concurrent users can share items which can be text, images, videos. Users of the service will be “authenticated”, and allowed several types of interactions (services) from SPPP.

<h3>Built with</h3>
Node.js<br>
Socket.io<br>
MySQL<br>
CSS

<h3>Use cases</h3>

Users 
-   registers
-   can view, put, get items into the chat

Admin
-   Authorize users
-   create new groups 

<h3>To do</h3>

Users 
-   share other binary files
-   edit and delete items from chat
-   create new groups

Admin
-   accept groups requested by user
-   delete items 
