var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var User = require('./models/user');
var Message = require('./models/message');
var cors = require('cors');
require('dotenv').config();
var socket = require("socket.io");
var cookie = require('cookie');
var signature = require('cookie-signature');
const { request } = require('express');
var siofu = require("socketio-file-upload");
var ss = require('socket.io-stream');
// var SocketIOFileUpload = require("socketio-file-upload");
var fs = require('fs');
var path = require('path');
var app = express(),
    store  = new session.MemoryStore(),
    secret = process.env.SECRET_KEY,
    name   = 'user_sid';
console.log("secr",process.env.SECRET_KEY);
// Global variables to hold all usernames and rooms created
var usernames = {};
var rooms = ["secure", "soccer", "alumni"];
app.set('port',process.env.PORT);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/scripts',express.static(__dirname + '/scripts'));
app.use(cookieParser());
app.use(session({
    name: name,
    secret: secret,
    resave: false,
    store:  store,
    saveUninitialized: false,
    cookie: {
        expires: 60000
    }
}));
app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});
var users = require('./models/users');
app.use(siofu.router);
app.use('/admindashboard',users);
app.use(cors());
app.use(bodyParser.json());

var sessionChecker = (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        res.redirect('/dashboard');
    } else {
        next();
    }    
};

app.get('/', sessionChecker, (req, res) => {
    res.redirect('/login');
});

app.route('/signup')
    .get(sessionChecker, (req, res) => {
        res.sendFile(__dirname + '/public/signup.html');
    })
    .post((req, res) => {
        console.log("fn",req.body.fullname);
        console.log("groups",req.body.groups);
        console.log("email",req.body.email);
        console.log("pass",req.body.password);
        console.log("Username",req.body.username);
        var fname = req.body.fullname;
        var groups = req.body.groups;
        var email = req.body.email;
        var passwd = req.body.password;
        var uname = req.body.username;
        var errorMessage = "";
        // const emailRegex = '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
        if(!(fname.length >= 2 && fname.length <= 80)){
            res.send(500,"please enter fullname within the specified range.")
        }
        if(!(uname.length >= 2 && uname.length <= 16)){
            res.send(500,"please enter username within the specified range.")
        }
        
        if(!(passwd.length >= 8 && passwd.length <= 20)){
            res.send(500,"please enter a password within the specified range.")
        }
        // if(!email.match(emailRegex)){
        //     res.send(500,"please enter a valid email.")
        // }
        if(groups == undefined){
            res.send(500,"please select a group you want to join.")
        }

        const usr = User.create({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            fullname: req.body.fullname,
            groups: req.body.groups,
            isauthenticated: false
        })
        .then(user => {
            console.log("user ",user.dataValues);
            // req.session.user = user.dataValues;
            res.redirect('/login');
        })
        .catch(error => {
            // alert("error",error.get());
            res.redirect('/signup');
        });
    });

app.route('/login')
.get(sessionChecker, (req, res) => {
    res.sendFile(__dirname + '/public/login.html');
})
.post((req, res) => {
    var username = req.body.username,
    password = req.body.password;
    if(!(username.length >= 2 && username.length <= 16)){
        res.send(500,"please enter username within the specified range.")
    }
    
    if(!(password.length >= 8 && password.length <= 20)){
        res.send(500,"please enter a password within the specified range.")
    }
    User.findOne({ where: { username: username } }).then(function (user) {
        if (!user) {
            res.redirect('/login');
        } else if (!user.isauthenticated) {
            res.json({status: "Not yet authorized by Admin", redirect: '/login'});
            // res.redirect('/login');
        }else if (!user.validPassword(password)) {
            res.redirect('/login');
        } else {
            req.session.user = user.username;
            req.session.usergroup = user.groups;
            if(username=='admin'){
                console.log("inside admin condition");
                res.redirect('/admindashboard');
            } else{
                io.on("createUser", function(username) {
                    io.emit("createUser", req.session.user, req.session.usergroup);
                  });
            res.redirect('/dashboard');}
        }
    });
});

app.get('/dashboard', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.sendFile(__dirname + '/public/dashboard.html');
    } else {
        res.redirect('/login');
    }
});

app.get('/admindashboard', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.sendFile(__dirname + '/public/admindashboard.html');
    } else {
        res.redirect('/login');
    }
});

app.get('/logout', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.redirect('/');
    } else {
        res.redirect('/login');
    }
});

app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
  });

var server = app.listen(app.get('port'), () => console.log(`App started on port ${app.get('port')}`));
var io = require('socket.io').listen(server);

io.on("connection", function(socket) {

    console.log("User connected to server.");
    // var req = socket.request;
    // console.log("Session: ", socket.handshake);
    // console.log("cookies: ", socket.handshake.cookie);
    // console.log("Req cookies: ",req);
    if (socket.handshake && socket.handshake.headers && socket.handshake.headers.cookie) {
        var raw = cookie.parse(socket.handshake.headers.cookie)[name];
        if (raw) {
          // The cookie set by express-session begins with s: which indicates it
          // is a signed cookie. Remove the two characters before unsigning.
          socket.sessionId = signature.unsign(raw.slice(2), secret) || undefined;
        }
      }
      if (socket.sessionId) {
        store.get(socket.sessionId, function(err, session) {
          console.log("session parsed from cookie: ",session);
          if(session!=undefined && session.user!=undefined && session.usergroup!=undefined){
            console.log("username parsed from cookie: ",session.user);
            console.log("user group parsed from cookie: ",session.usergroup[0]);
            console.log("socket.client.id: ",socket.client.id);
            socket.username = session.user;
                socket.groups = session.usergroup;
                let username = session.user;
                usernames[session.user] = session.user;
                socket.currentRoom = session.usergroup[0];
                socket.join(session.usergroup[0]);
                socket.emit("updateChat", "INFO", "You have joined "+session.usergroup[0]+" group room");
                socket.broadcast
                    .to(session.usergroup[0])
                    .emit("updateChat", "INFO", session.user + " has joined global room");
                io.sockets.emit("updateUsers", username.toArray);
                socket.emit("updateRooms", session.usergroup, "global");
            }
                });
        }

    var uploader = new siofu();
    uploader.dir = __dirname + "/uploads";
    uploader.listen(socket);
    // uploader.on('start', function(event) {
    //     console.log(event.file.bytesLoaded / event.file.size)
    //     console.log("event file",event.file);
    //     socket.emit('upload.progress', {
    //       percentage:(event.file.bytesLoaded / event.file.size) * 100
    //     })
    //     socket.emit('updateChat',"INFO", socket.username + " has uploaded a file "+event.file.name)
    //     var readStream = fs.createReadStream(path.resolve(__dirname+'/uploads/', event.file.name), {
    //         encoding: 'base64'
    //     }), chunks = [];

    //     readStream.on('readable', function(){
    //         console.log('Img loading');
    //     })

    //     readStream.on('data', function(chunk){
    //         chunks.push(chunk);
    //         socket.emit('img-chunk',chunk);
    //     });
    //     readStream.on('end', function(){
    //         console.log('ing loaded');
    //     })
    //     socket.emit('image-uploaded', event.file.name);
        
    //     });
          
    // socket.on("createUser", function(username, groups) {
    //     console.log("inside sock username: ",username);
    //     console.log("Inside sock username: ",groups);
    //   socket.username = username;
    //   socket.groups = groups;
    //   usernames[username] = username;
    //   socket.currentRoom = "secure";
    //   socket.join("secure");
    //   socket.emit("updateChat", "INFO", "You have joined secure group room");
    //   socket.broadcast
    //     .to("secure")
    //     .emit("updateChat", "INFO", username + " has joined global room");
    //   io.sockets.emit("updateUsers", usernames);
    //   socket.emit("updateRooms", rooms, "global");
    // });

    socket.on("sendMessage", function(data) {
        console.log("data len",data.length);
        if(data.length >0 && data.length<=140){
            const msg = Message.create({
                message: data,
                sentBy: socket.username,
                messageType: "text",
                messageGroup: "global"
            }).then(message => {
                console.log("message text after db insert: ",message);
                io.sockets
                .to(socket.currentRoom)
                .emit("updateChat", socket.username, data); 
            }).catch(error => {
                alert("error",error.get());
            });
            
        }
        
      });

    socket.on("updateRooms", function(room,currentRoom) {
    socket.broadcast
        .to(currentRoom)
        .emit("updateChat", "INFO", socket.username + " left room");
    socket.leave(currentRoom);
    socket.currentRoom = room;
    socket.join(room);
    socket.emit("updateChat", "INFO", "You have joined " + room + " room");
    socket.broadcast
        .to(room)
        .emit("updateChat", "INFO", socket.username + " has joined " + room + " room");
    });

    socket.on("disconnect", function() {
    delete usernames[socket.username];
    io.sockets.emit("updateUsers", usernames);
    socket.broadcast.emit("updateChat", "INFO", socket.username + " has disconnected");
    });
    
    // SHARE AN IMAGE OR A VIDEO
    socket.on('file', function(dataURI,type){
        
        // var to = user.peers;
       
        // for(var i=0; i < to.length; i++){
        //     dir[to[i]].

        //         socket.emit('upload.progress', {
        //   percentage:(dataURI.file.bytesLoaded / dataURI.file.size) * 100
        // })

        // console.log(readStream);
        // console.log("path: ",__dirname+'/uploads/');
        // console.log("dataURI: ",dataURI);

        // const msg = Message.create({
        //     message: "binary",
        //     messageblob: dataURI,
        //     sentBy: socket.username,
        //     messageType: type,
        //     messageGroup: "global"
        // }).then(message => {
        //     console.log("message img/video after db insert: ",message);
        //     io.emit('file', dataURI,type, socket.username);
        // }).catch(error => {
        //     console.log("error",error);
        // });

        io.emit('file', dataURI,type, socket.username);
        // }
        console.log(socket.username + ' is sharing a file');
    });

    socket.on('fileBinary', function(dataURI,type){
        
        // var to = user.peers;
       
        // for(var i=0; i < to.length; i++){
        //     dir[to[i]].
        // console.log("dataURI,type",dataURI);

        const msg = Message.create({
            message: dataURI,
            sentBy: socket.username,
            messageType: type,
            messageGroup: "global"
        }).then(message => {
            console.log("message binary after db insert: ",message);
            io.emit('fileBinary', dataURI,type, socket.username);
        })

        console.log("type",type);

        // io.emit('fileBinary', dataURI,type, socket.username);
        // }
        console.log(socket.username + ' is sharing a file');
    });

    socket.on('sendmeafile', function () {
        var stream = ss.createStream();
        stream.on('end', function () {
            console.log('file sent');
        });
        console.log("uploader.dir+\"/SPPP.pdf",uploader.dir+"/SPPP.pdf");
        ss(socket).emit('sending', stream); 
        fs.createReadStream(uploader.dir+"/SPPP.pdf").pipe(stream);
      }); 
    
    });