var Sequelize = require('sequelize');
var bcrypt = require('bcrypt');
// require('dotenv').config();

//Cloud
var sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USER,process.env.DB_PASSWORD,{
    host: process.env.INSTANCE_CONNECTION_NAME,
    dialect: 'mysql'
    ,dialectOptions: {
        socketPath: process.env.INSTANCE_CONNECTION_NAME,
    },
});

// var sequelize = new Sequelize('mysql://root:Root@123@localhost:3306/sppp_db');
try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }

var Message = sequelize.define('messages',{
    
    message: {
        type: Sequelize.STRING,
        allowNull: false
    },
    messageblob: {
        type: Sequelize.BLOB('long'),
        allowNull: true
    },
    sentBy: {
        type: Sequelize.STRING,
        allowNull: false
    },
    messageType: {
        type: Sequelize.STRING,
        allowNull: false
    },
    messageGroup: {
        type: Sequelize.STRING,
        allowNull: false
    },
} );

try{
    sequelize.sync();
    console.log('messages table has been successfully created, if one doesn\'t exist');
} catch(error) {
    console.log('This error occured', error)
}

module.exports = Message;