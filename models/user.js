var Sequelize = require('sequelize');
var bcrypt = require('bcrypt');
// require('dotenv').config();

// var sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USER,process.env.DB_PASSWORD,{
//     host: process.env.DB_HOST,
//     port: process.env.DB_PORT,
//     dialect: 'mysql',
//     pool: {
//         max: 2,
//         min: 0,
//         idle: 10000
//     }
//   });

//Cloud
var sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USER,process.env.DB_PASSWORD,{
    host: process.env.INSTANCE_CONNECTION_NAME,
    dialect: 'mysql'
    ,dialectOptions: {
        socketPath: process.env.INSTANCE_CONNECTION_NAME,
    },
});

// var sequelize = new Sequelize('mysql://root:Root@123@localhost:3306/sppp_db');
try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }

var User = sequelize.define('appusers',{
    
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        isEmail: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    fullname: {
        type: Sequelize.STRING,
        allowNull: false
    },
    isauthenticated: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    groups: {
        type: Sequelize.STRING,
        allowNull: false,
        get() {
            return this.getDataValue('groups').split(';')
        },
        set(val) {
            if(Array.isArray(val)){
                this.setDataValue('groups',val.join(';'));
            } else {
                this.setDataValue('groups',val);
            }
           
        }
    },
}   , {
    hooks: {
        beforeCreate: (user) => {
            const salt = bcrypt.genSaltSync();
            user.password = bcrypt.hashSync(user.password, salt);
        }
    }
    
});

User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

try{
    sequelize.sync();
    console.log('appusers table has been successfully created, if one doesn\'t exist');
} catch(error) {
    console.log('This error occured', error)
}

module.exports = User;