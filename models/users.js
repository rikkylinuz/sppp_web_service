var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var bodyParser = require('body-parser');
router.use(bodyParser.json());
require('dotenv').config();

// var sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USER,process.env.DB_PASSWORD,{
//   host: process.env.DB_HOST,
//   port: process.env.DB_PORT,
//   dialect: 'mysql',
//   pool: {
//       max: 2,
//       min: 0,
//       idle: 10000
//   }
// });


//Cloud
var sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USER,process.env.DB_PASSWORD,{
  host: process.env.INSTANCE_CONNECTION_NAME,
  dialect: 'mysql'
  ,dialectOptions: {
      socketPath: process.env.INSTANCE_CONNECTION_NAME,
  },
});

// var sequelize = new Sequelize('mysql://root:Root@123@localhost:3306/sppp_db');
// another routes also appear here
// this script to fetch data from MySQL databse table
router.get('/users/authUsers', async function(req, res) {
    // var sequelize = new Sequelize('mysql://root:Root@123@localhost:3306/sppp_db');
    var sql='SELECT `username`,`isauthenticated`,`groups`,`createdAt` FROM appusers where isauthenticated=0;';
    return new Promise((resolve, reject) => {
      sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} , function (err, data, fields) {
        // console.log("result for unauth users res.json(data) :", res.json(data));
        console.log("result for unauth users:", data);
        if (err) throw err;
        // res.json(data);
        // Promise.all(res.json("data", data));
        // next();
      }).then(result => {
        console.log(result);
        console.log('DB isauth id is:0 ' + result[0].username);
        console.log('DB isauth id is:1 ' + result[0].isauthenticated);

        if (result[0].isauthenticated != 1) {
          console.log('True block');
          is_matched = true;
        } else {
          is_matched = false;
          reject(true);
        }
        resolve(is_matched);
        res.json(result);
      });
    })
});

router.put('/users/authUsers/', function(req, res) { 
  console.log('inside update auth status req.username',req);
  console.log("1",req.body);
  console.log("2",req.body.username);
  console.log("3",req.headers['content-type']);
  // var sequelize = new Sequelize('mysql://root:Root@123@localhost:3306/sppp_db');
  var sql='UPDATE appusers set isauthenticated = 1 where username=$1;';
  return new Promise((resolve, reject) => {
      sequelize.query(sql,{bind: [req.body.username], type: Sequelize.QueryTypes.UPDATE}, function (err, data, fields) {
      if (err) throw err;
      // res.json(data);
    }).then(result => {
      console.log("update result",result);
    });
  });
  
});
module.exports = router;