var getUsersId = document.getElementById("usercontainer");
getUsersId.addEventListener("click", getUsers());

class EasyHTTP { 

    // Make an HTTP PUT Request 
    async put(url, data) { 
        // Awaiting fetch which contains method, 
        // headers and content-type and body 
        const response = await fetch(url, { 
        method: 'PUT', 
        headers: { 
            // Accept: 'application/json',
            'Content-Type': 'application/json',
        }, 
        // mode: "cors",
        body: JSON.stringify(data),
        }); 
        
        // Awaiting response.json() 
        const resData = await response.json(); 
    
        // Return response data 
        return resData; 
    } 
    }

async function getUsers(){
    var arr = await fetch('https://sp-share-file-chatapp.uc.r.appspot.com/admindashboard/users/authUsers').then(response => {
        // var arr = await fetch('http://localhost:8082/admindashboard/users/authUsers').then(response => {
         // Logs the response
        return response.json();
    });
    var cont = document.getElementById('usercontainer');

    var userTable = document.createElement("table");
    var tableBody = userTable.createTBody();
    for (i = 0; i <= arr.length - 1; i++) {

        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.setAttribute('style', 'padding-right: 40px;, margin: 20px;');
        // var p = document.createElement("span");
        // p.setAttribute('class','uname');
        // p.append(arr[i].username);
        td.appendChild(document.createTextNode(arr[i].username+"                "));
        // td.appendChild(p);
        td.appendChild(document.createTextNode(arr[i].groups+"               "));
        td.appendChild(document.createTextNode(arr[i].createdAt+"                "));
        var authBtn = document.createElement("button");
        authBtn.setAttribute("id","authId");
        authBtn.setAttribute('style', 'height: 20px;');
        authBtn.setAttribute('val',arr[i].username);
        
        td.appendChild(authBtn);
        tr.appendChild(td);
        tableBody.appendChild(tr);
    }

    cont.appendChild(userTable);
    // document.getElementById("getUsers").onclick() == null;
    var authorizeBtn = document.getElementById("authId");
    authorizeBtn.addEventListener('click', function(){
    var val = authorizeBtn.getAttributeNode("val").value;
    // Make an HTTP PUT Request 
    const http = new EasyHTTP;
    
    const reqdata = { "username": val};
    // Update Post 
    http.put( 
    // 'http://localhost:8082/admindashboard/users/authUsers', 
    'https://sp-share-file-chatapp.uc.r.appspot.com/admindashboard/users/authUsers',
    reqdata) 
      
    // Resolving promise for response data 
    .then(data => console.log(data)) 
      
    // Resolving promise for error 
    .catch(err => console.log(err));
    });
    
}

