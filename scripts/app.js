'use strict';
// var socket = io.connect("http://localhost:8082");
var socket = io.connect("https://sp-share-file-chatapp.uc.r.appspot.com");
var SocketIOFileUpload = require("socketio-file-upload");
var userlist = document.getElementById("userlist");
var roomlist = document.getElementById("roomlist");
var message = document.getElementById("message");
var sendMessageBtn = document.getElementById("send");
var shareFileBtn = document.getElementById("share");
// var createRoomBtn = document.getElementById("create-room");
var messages = document.getElementById("msg");
var images = document.getElementById("imgId");
var editId = document.getElementById("editid");
var chatDisplay = document.getElementById("chat-display");
var ss = require('socket.io-stream');
var fs = require('browserify-fs');
// var currentRoom = "global";
var MAX_UPLOAD_SIZE = 5;
// Send message on button click
sendMessageBtn.addEventListener("click", function () {
  socket.emit("sendMessage", message.value);
  message.value = "";
});

// editId.addEventListener("click", function () {

//   socket.emit("editMessage", message.value);
//   message.value = "";
// });

// Send message on enter key press
message.addEventListener("keyup", function (event) {
  if (event.keyCode == 13) {
    sendMessageBtn.click();
  }
});

socket.on("updateChat", function(username, data) {
  if (username == "INFO") {
    messages.innerHTML +=
      "<p class='alert alert-warning w-100'>" + data + "</p>" ;
      
  }
  else {
    let date = new Date(Date.now());
    messages.innerHTML +=
      "<p><span><strong>" + username + ": </strong></span>" + data + "<button id=\"editid\"> edit</button>"+"<br>"+ date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+" "+(Number(date.getMonth())+Number(1))+"/"+date.getDate()+"</p>";
  }

  chatDisplay.scrollTop = chatDisplay.scrollHeight;
});


socket.on("updateUsers", function(usernames) {
  userlist.innerHTML = "";

  for (var user in usernames) {
    userlist.innerHTML += "<li>" + user + "</li>";
  }
});


socket.on("updateRooms", function(rooms, newRoom) {
  roomlist.innerHTML = "";

  for (var index in rooms) {
    roomlist.innerHTML +=
      '<li class="rooms" id="' +
      rooms[index] +
      '" onclick="changeRoom(\'' +
      rooms[index] +
      "')\"># " +
      rooms[index] +
      "</li>";
  }

  // if (newRoom != null) {
  //   document.getElementById(newRoom).classList.add("text-warning");
  // } else {
  //   document.getElementById(currentRoom).classList.add("text-warning");
  // }

});


// I    SHARE Image and video
// source: http://www.sitepoint.com/html5-file-drag-and-drop/
var imageReader = new FileReader();
var videoReader = new FileReader();
var file;

$('#fileselect').change(function(e){
    
    // get file object from file selector input
    file = e.target.files[0];   

});



$('#upload').submit(function(){

    if (file){
   
        if (file.type.substring(0,5) === 'image' || file.type.substring(0,5) === 'video'){
        
            if (file.size > MAX_UPLOAD_SIZE * 1000 * 1000)
            {
                alert('Sorry, we can only accept files up to ' + MAX_UPLOAD_SIZE + ' MB');
            }
            else if (file.type.substring(0,5) === 'image'){
                
                // upload image  
                imageReader.readAsDataURL(file);
            }
            else if (file.type.substring(0,5) === 'video'){
                
                // uplaod video  
                videoReader.readAsDataURL(file);
            }
        }
        else {
            alert("Sorry, you an only share images or videos");
        }

        // reset select box and file object 
        $('#fileselect').val('');
        file = '';
    }
    else
    {
        alert("You haven't selected any file to share");
    }
    return false; // don't reload the page
});


imageReader.onload=function(e){
    
    // append image to own interface
    // appendFile(e.target.result,'image','self');
    // scrollDown();
    
    // share image
    // TODO try stream?
    socket.emit('file',e.target.result,'image');
};

videoReader.onload=function(e){
    
    // append video to own interface
    // appendFile(e.target.result,'video','self');
    // scrollDown();
    
    // share video
    socket.emit('file',e.target.result,'video');
};

socket.on('file', function(dataURI,type,from){
    
    appendFile(dataURI,type,from);
    // scrollDown();

});


// I    SHARE Files
// source: http://www.sitepoint.com/html5-file-drag-and-drop/
var fileReader = new FileReader();
var fileBinary;
var fileBinaryType;

$('#fileselectBinary').change(function(e){
    
    // get file object from file selector input
    fileBinary = e.target.files[0];   
    
});

var uploader = new SocketIOFileUpload(socket);
                uploader.listenOnSubmit(document.getElementById('shareBinary'),document.getElementById('fileselect'));

$('#uploadBinary').submit(function(){

    if (fileBinary){
      fileBinaryType = fileBinary.type;
      console.log("fileBinary.type: ",fileBinary.type);
        if (fileBinary.type.match('application/*')){
        
            if (fileBinary.size > MAX_UPLOAD_SIZE * 1000 * 1000)
            {
                alert('Sorry, we can only accept files up to ' + MAX_UPLOAD_SIZE + ' MB');
            }
            else {
                
                // upload image  
                // fileReader.readAsArrayBuffer(fileBinary);
                fileReader.readAsBinaryString(fileBinary);
                // fileReader.readAsDataURL(fileBinary);

                // uploader.listenOnInput(document.getElementById('fileselect'));
                uploader.on("progress", function(event){
                  console.log("Error from uploader", event);
    });
            }
            
        }
        else {
            alert("Sorry, you an only share doc,docx,pdf.");
        }

        // reset select box and file object 
        // $('#fileselectBinary').val('');
        // fileBinary = '';
    }
    else
    {
        alert("You haven't selected any file to share");
    }
    return false; // don't reload the page
});

fileReader.onload=function(e){
    
    // append video to own interface
    // appendFile(e.target.result,'video','self');
    // scrollDown
    // console.log("e.target.result: ", e.target.result);
    console.log("e.target.result type: ", e.target.type);
    // share video
    socket.emit('fileBinary',e.target.result,fileBinaryType);
    fileBinaryType='';
};

socket.on('fileBinary', function(dataURI,type,from){
  console.log("dataUri: ",dataURI);
  console.log("type: ",type);
  console.log("from: ",from);
  appendFileBinary(dataURI,type,from);
    // scrollDown();
  // socket.emit('sendmeafile');
});

function changeRoom(room) {

  if (room != currentRoom) {
    socket.emit("updateRooms", room, currentRoom);
    document.getElementById(currentRoom).classList.remove("text-warning");
    currentRoom = room;
    document.getElementById(currentRoom).classList.add("text-warning");
  }

}

// Appends either an image or a video file to user's chat window
function appendFile(URI,type,user){
  let date = new Date(Date.now());
  if (user === 'self'){
      $('#msg').append($('<p><span><strong> style="color:gray; font-weight: 100;">').text('You:'));
  }
  else {
      $('#msg').append($('<p><span><strong>').text(user + ':'));
  }
  if (type === 'image'){
      $('#msg').append('<p><img src="' + URI + '" height="150px" />'+ "<button id=\"editid\"> edit</button>"+ "<br>"+ date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+" "+(Number(date.getMonth())+Number(1))+"/"+date.getDate() +'</p>');
  }
  else {
      $('#msg').append('<p><video width="320" height="240" controls><source src="' + URI + '"></p>');
  }
}

function appendFileBinary(URI,type,user){
  let date = new Date(Date.now());
      console.log("type of binary file in append binary file: ",type);
      var data = fs.writeFile(URI);
      $('#msg').append($('<p><span><strong>').text(user + ':'));

      $('#msg').append('<p><object data="' + sfsd.pdf + ' type="'+type+'" height="150px" />'+ "<br>"+ date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+" "+(Number(date.getMonth())+Number(1))+"/"+date.getDate() +'</p>');
      // <embed src="file_name.pdf" width="800px" height="2100px" />
      $('#fileselectBinary').val('');
      fileBinary = '';
}

ss(socket).on('sending', function(stream) {
  stream.pipe(fs.wcreateWriteStream("/home/rlf/sppp_web_service/uploads/SPPP.pdf")); 
  stream.on('end', function () {
    console.log('file received');
  });
});


